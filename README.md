# Sample Moodle configuration files

This project demonstrates a simple method for using file-based configuration in
moodle.  This sample is based on our own configuration at the University of
Wisconsin - Madison.

This project is an extract of a working configuration, and is intended as an
example.  To use this with a given site, you'll need to modify the included php
files to suit your own needs.

__A working knowlege of php development is essential.__

## Using the included sample configuration:

1. Clone the git project into a directory accessible by the webserver.
This directory should probably be outside the webserver root for security reasons;
none of the configuration files are directly accessed from the web.
2. Add the following lines to Moodle's config.php immediately __before__ the last
`require_once`.

```php
// Include site config
require_once('path-to-config/local_moodle_config.class.php');
local_moodle_config::load_config('3.0', 'mysite.mydomain.org');
```
Here _path-to-config_ is the absolute path to the configuration directory,
3.0 is the moodle version you are running
and _mysite.mydomain.org_ is some identifier for your site.

Finally, edit the included configuration to suit your needs.


## Configuration paths:

The `load_config` function will search for a directory
`path-to-config/moodle/3.0/base.php`.
The base.php file must exist and will contain the base configuration for Moodle
3.0 sites.  After loading that file, it will search for a site-specific file:
`path-to-config/moodle/3.0/sites/mysite.mydomain.org.php`.
If that file exists, then it'll load it after the base.php file.

We load the site-specific files after the base configuration so the site
configuration can override individual settings.

The parameters to `load_config` are useful to allow the same configuration
directory to be used for different moodle sites, and can be set to anything
convenient to a particular setup.  There is no requirement that the version
is really the moodle version or that the site identifier is a DNS name.

For example, this is valid:

```php
// Include site config
require_once('path-to-config/local_moodle_config.class.php');
local_moodle_config::load_config('standard', 'production_site_1');
```

Just make sure that there is a corresponding configuration `path-to-config/moodle/standard/base.php`
and optionally `path-to-config/moodle/standard/sites/production_site_1.php`.



# Autoconfiguration plugin

This plugin allows additional moodle configuration options which require PHP code.
For example, it's possible to automatically provision roles, lti tools, filters,
and language packs.

## Installation

The autoconfiguration plugin is avaiable here: https://git.doit.wisc.edu/uw-moodle/moodle-local_autoconfig.git

1. Clone the plugin into `local/autoconfig`.
2. Install the plugin in the moodle UI.
3. Configure the plugin by modifying the included moodle/3.0/autoconfig.php file.
See the sample file and code comments in the plugin itself to see what's possible.
Roles can be added by exporting an existing role to xml and placing it in the roles
subdirectory.
4. Run the autoconfiguration.  This is possible from the moodle UI and a CLI script.


**Note that running this script will change your site configuration!  Don't do
this blindly on a production site!**

For more information, see the readme contained in the plugin.

