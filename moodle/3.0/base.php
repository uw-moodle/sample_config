<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Sample base configuration file
 *
 * @copyright  2016 University of Wisconsin
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// Default blocks configuration
$CFG->defaultblocks_override = 'calendar_month,news_items:participants,messages';

// Advanced features
$CFG->enableportfolios = 1;
$CFG->enablewebservices = 1;
$CFG->enablecompletion = 1;
$CFG->enableavailability = 1;

// Users / Permissions / User policies
$CFG->hiddenuserfields = 'icqnumber,skypeid,yahooid,aimid,msnid,firstaccess,lastaccess,mycourses,suspended';
$CFG->enablegravatar = 1;

// Courses / Course default settings
$CFG->forced_plugin_settings['moodlecourse']['visible'] = 0;
$CFG->forced_plugin_settings['moodlecourse']['maxbytes'] = 209715200; // 200MB
$CFG->forced_plugin_settings['moodlecourse']['enablecompletion'] = 1;

// Grades / General settings
$CFG->grade_export_userprofilefields = 'firstname,lastname,idnumber,username,institution,department,email';
$CFG->grade_export_customprofilefields = 'emplid,netid,photoid';
$CFG->recovergradesdefault = 1;
$CFG->unlimitedgrades = 1;
$CFG->gradepointmax = 1000;

// Grades / Grade category settings
$CFG->grade_aggregations_visible = '0,2,4,6,8,10,11,12,13';  // See lib/grade/constants.php

// Grades / Report settings / Grader report
$CFG->grade_report_studentsperpage = 50;

// Location / Location settings
$CFG->country = 'US';
$CFG->defaultcity = 'Madison, WI';

// Language / Language settings
$CFG->lang = 'en_us';
$CFG->langmenu = 0;

// Security / Site policies
$CFG->opentogoogle = 1;
$CFG->cronclionly = 1;
$CFG->cronremotepassword = 'supersecret';
$CFG->maxbytes = 209715200; // 200MB

// Security / HTTP security
$CFG->loginhttps = 0;
$CFG->cookiesecure = 0;

// Appearance / Additional HTML
$CFG->additionalhtmlfooter = '';  // local/googleanalytics abuses this field, so reset to empty every time

// Appearance / Additional HTML
$CFG->defaulthomepage = 1;  // 0 = Site, 1 = My Home, 2 = User preference

// Appearance / Themes / Theme selector
$CFG->theme = 'clean';

// Front page / Front page settings
$CFG->frontpage = '4,7';  // Combo list, Course search box
$CFG->frontpageloggedin = '4,7';  // Combo list, Course search box
$CFG->maxcategorydepth = 1;

// Server / System paths
$CFG->pathtodu = '/usr/bin/du';
$CFG->aspellpath = '/usr/bin/aspell';
$CFG->pathtodot = '/usr/bin/dot';

// Server / Support contact
$CFG->supportname = 'Moodle Support';
$CFG->supportemail = 'moodleadmins@doesntexist.com';
$CFG->supportpage = 'https://docs.moodle.org/30/en/Main_page';

// Server / Session handling
$CFG->dbsessions = 1;
$CFG->sessiontimeout = 3*3600;

// Server / Update notifications
$CFG->updateautocheck = 0;

// Development / Experimental
$CFG->enablegroupmembersonly = 1;

// Development / Debugging
$CFG->debug = 15; // NORMAL
$CFG->debugdisplay = 0;

// Plugins / Activity modules / Assignment /Submission plugins / File submissions
$CFG->forced_plugin_settings['assignsubmission_file']['maxbytes'] = 20971520; // 20MB

// Plugins / Activity modules / Book
$CFG->forced_plugin_settings['book']['requiremodintro'] = 0;

// Plugins / Activity modules / Folder
$CFG->forced_plugin_settings['folder']['requiremodintro'] = 0;

// Plugins / Activity modules / Glossary
$CFG->glossary_fullmatch = 1;

// Plugins / Activity modules / IMS content package
$CFG->forced_plugin_settings['imscp']['requiremodintro'] = 0;

// Plugins / Activity modules / Mediasite content
$CFG->mediasite_restricttoip = 0;

// Plugins / Activity modules / Page
$CFG->forced_plugin_settings['page']['requiremodintro'] = 0;

// Plugins / Activity modules / File
$CFG->forced_plugin_settings['resource']['requiremodintro'] = 0;
$CFG->forced_plugin_settings['resource']['displayoptions'] = '0,1,2,3,4,5,6'; // ALL

// Plugins / Activity modules / URL
$CFG->forced_plugin_settings['url']['requiremodintro'] = 0;
$CFG->forced_plugin_settings['url']['displayoptions'] = '0,1,2,3,5,6'; // ALL

// Plugins / Activity modules / Quiz
$CFG->forced_plugin_settings['quiz']['autosaveperiod'] = '120';
// Clear some advanced flags.  We have to specify both the setting and the setting_adv to make this work.
$CFG->forced_plugin_settings['quiz']['attemptonlast'] = '0';
$CFG->forced_plugin_settings['quiz']['attemptonlast_adv'] = '';
$CFG->forced_plugin_settings['quiz']['attempts'] = '1';
$CFG->forced_plugin_settings['quiz']['browsersecurity'] = '-';
$CFG->forced_plugin_settings['quiz']['browsersecurity_adv'] = '';
$CFG->forced_plugin_settings['quiz']['delay1'] = '0';
$CFG->forced_plugin_settings['quiz']['delay1_adv'] = '';
$CFG->forced_plugin_settings['quiz']['delay2'] = '0';
$CFG->forced_plugin_settings['quiz']['delay2_adv'] = '';
$CFG->forced_plugin_settings['quiz']['password'] = '';
$CFG->forced_plugin_settings['quiz']['password_adv'] = '';
$CFG->forced_plugin_settings['quiz']['subnet'] = '';  // Subnet should probably be advanced, but it's not at the bottom of the group
                                                      // so having it advanced is a little wonky in the UI.
$CFG->forced_plugin_settings['quiz']['subnet_adv'] = '';

// Plugins / Message outputs / Default message outputs
$CFG->forced_plugin_settings['message']['message_provider_moodle_instantmessage_loggedin'] = 'popup,email';

// Plugins / Authentication / Manage authentication
//$CFG->auth = 'shibboleth';  // comma-separated list of enabled auth methods.
                            // Changes require cache purge.
$CFG->registerauth = 0;

// Plugins / Authentication / Manual
$CFG->forced_plugin_settings['auth/manual']['field_lock_firstname'] = 'unlockedifempty';
$CFG->forced_plugin_settings['auth/manual']['field_lock_lastname'] = 'unlockedifempty';
$CFG->forced_plugin_settings['auth/manual']['field_lock_idnumber'] = 'locked';

// Plugins / Enrolments / Manage enrol plugins
$CFG->enrol_plugins_enabled = 'manual,guest,self,cohort,meta,flatfile'; // Changes require cache purge

// Plugins / Enrolments / Self enrolment
$CFG->forced_plugin_settings['enrol_self']['expiredaction'] = 0; // Unenroll user from course

// Plugins / Text editors / TinyMCE HTML editor / Legacy spell checker
$CFG->forced_plugin_settings['tinymce_spellchecker']['spellengine'] = 'PSpell';

// Plugins / Text editors / Atto editor / atto_preview
$CFG->forced_plugin_settings['atto_preview']['layout'] = 'embedded';

// Plugins / Filters / MathJaxLoader
$CFG->forced_plugin_settings['filter_mathjaxloader']['texfiltercompatibility'] = 1;
$CFG->forced_plugin_settings['filter_mathjaxloader']['httpsurl'] = 'https://cdn.mathjax.org/mathjax/2.6-latest/MathJax.js';
$CFG->forced_plugin_settings['filter_mathjaxloader']['httpurl'] = 'http://cdn.mathjax.org/mathjax/2.6-latest/MathJax.js';

// Plugins / Filters / TeX notation
$CFG->forced_plugin_settings['filter_tex']['convertformat'] = 'png';
$CFG->forced_plugin_settings['filter_tex']['latexpreamble'] = <<<'EOF'
\usepackage[latin1]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\RequirePackage{amsmath,amssymb,latexsym}
\newcommand{\ds}{\displaystyle}
EOF;

// Plugins / Filters / Convert URLs
$CFG->forced_plugin_settings['filter_urltolink']['formats'] = '0,1';  // Moodle auto + HTML

// Plugins / Web services / Manage protocols
$CFG->webserviceprotocols = 'rest,soap';

// Include autoconfig setup if needed.
// This is only used by the local_autoconfig plugin.
if (defined('LOCAL_AUTOCONFIG')) {
    include(__DIR__. 'autoconfig.php');
}