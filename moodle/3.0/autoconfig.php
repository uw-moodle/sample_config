<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Sample autoconfiguration file.
 *
 * This file provides configuration settings for the local_autoconfig plugin.
 *
 * @copyright  2016 University of Wisconsin
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$CFG->local_autoconfig = new stdClass;

// Site admins by username.
// Always include 'admin' first
$CFG->local_autoconfig->siteadmins = "admin,another_admin";

// Language packs
$CFG->local_autoconfig->languagepacks_install = 'en,en_us,ja,zh_cn';

// Create some additional course categories.
$CFG->local_autoconfig->categories[] = array(
    'name'=>'Division of Continuing Studies',
    'idnumber'=>'DCS',
    'description'=>'',
    'parentidnumber'=>'');

// Install all roles in roles/ directory
$CFG->local_autoconfig->roles_definition_dir = __DIR__. '/roles';

// Set config settings which depend on role shortnames
$CFG->local_autoconfig->gradebookroles_byshortname = 'student,student_hidden';
$CFG->local_autoconfig->profileroles_byshortname = 'editingteacher,teacher,student,grader';
$CFG->local_autoconfig->report_usage_roles_byshortname = 'student,student_hidden';
$CFG->local_autoconfig->coursecontact_byshortname = 'editingteacher';
$CFG->local_autoconfig->block_quickmail_roleselection_byshortname = 'editingteacher,teacher,student,student_hidden,advisor,grader,invitedguest,tutor,accessibility';

// Set the role sort order.  Any roles not listed here are left at the end.
$CFG->local_autoconfig->role_sort_order = 'manager,editingteacher,teacher,student,student_hidden,contenteditor';

// Filter enable/disable
$CFG->local_autoconfig->filter_on = 'mediaplugin,mathjaxloader,tex';
$CFG->local_autoconfig->filter_off = 'urltolink,activitynames,glossary';
$CFG->local_autoconfig->filter_disabled = 'algebra,censor,data,emailprotect,emoticon,multilang,tidy';
$CFG->local_autoconfig->filter_order = 'mediaplugin,mathjaxloader,tex,urltolink,activitynames,glossary';

// Block enable/disable
$CFG->local_autoconfig->block_enable = '';
$CFG->local_autoconfig->block_disable = 'online_users,feedback';

// Activity enable/disable
$CFG->local_autoconfig->activity_enable = 'feedback';
$CFG->local_autoconfig->activity_disable = '';

// Grade scales
$CFG->local_autoconfig->gradescales[] = array(
        'name'=>'UW Course Grading System',
        'scale'=>'F, D, C, BC, B, AB, A',
        'description'=>'The standard UW grading scale.');
$CFG->local_autoconfig->gradescales[] = array(
        'name'=>'Yes/No (2 point)',
        'scale'=>'No, Yes',
        'description'=>'');

// Pre-create anon users.
// This is useful in allowing instructors to do anonymized course restores without also giving them
// the ability to create new users.  Set this high enough to allow for all course restores.  (We use 2000)
$CFG->local_autoconfig->anonusers = 20;

// Caching config
//
//    $CFG->local_autoconfig->cacheinstances[] = array(
//            'plugin'=>'file',
//            'name'=>'localcache',
//            'lock'=>'cachelocal_file_default',
//            'path'=>'/var/tmp/moodlecache',
//            'autocreate'=>1,
//            'mappings' => array('core/string','core/langmenu','core/databasemeta','core/htmlpurifier'));


// Site news feed
$CFG->local_autoconfig->rssnewsfeed = array(
        'url'=>'https://moodle.org/rss/file.php/51/1b51bf7f3cab9689af042af1ff4a07f0/mod_forum/1/rss.xml',
        'autodiscovery'=>1,
        'preferredtitle'=>'Moodle News',
        'title'=>'Moodle - News and Announcements',
        'description'=>'News and Announcements',
        'shared'=>'1');

// Block configuration for site and default my moodle page
$CFG->local_autoconfig->defaultblocks_site = 'rss_client:participants,messages';
$CFG->local_autoconfig->defaultblocks_my = 'rss_client:course_overview,calendar_upcoming,messages,private_files';

// Repository config

// Most of these require credentials, obviously.

// $CFG->local_autoconfig->repositories[] = array(
//         'repos'=>'boxnet',
//         'pluginname'=>'',
//         'clientid'=>$CFG->boxnet_clientid,
//         'clientsecret'=>$CFG->boxnet_clientsecret,
//         'visible'=>1);

// $CFG->local_autoconfig->repositories[] = array(
//         'repos'=>'googledocs',
//         'pluginname'=>'',
//         'clientid'=>$CFG->googledocs_clientid,
//         'secret'=>$CFG->googledocs_clientsecret,
//         'visible'=>1);

// $CFG->local_autoconfig->repositories[] = array(
//         'repos'=>'youtube',
//         'pluginname'=>'',
//         'apikey'=>$CFG->googleapi_key,
//         'visible'=>1);

$CFG->local_autoconfig->repositories[] = array(
        'repos'=>'filesystem',
        'enablecourseinstances'=>1,
        'enableuserinstances'=>1,
        'visible'=>1);
$CFG->local_autoconfig->repositories[] = array(
        'repos'=>'poodll',
        'enablecourseinstances'=>1,
        'enableuserinstances'=>0,
        'visible'=>1);

// Portfolio config

// $CFG->local_autoconfig->portfolios[] = array(
//         'plugin'=>'boxnet',
//         'name'=>'Box',
//         'clientid'=>$CFG->boxnet_clientid,
//         'clientsecret'=>$CFG->boxnet_clientsecret,
//         'visible'=>1);

// $CFG->local_autoconfig->portfolios[] = array(
//         'plugin'=>'googledocs',
//         'name'=>'Google Drive',
//         'clientid'=>$CFG->googledocs_clientid,
//         'secret'=>$CFG->googledocs_clientsecret,
//         'visible'=>1);

$CFG->local_autoconfig->portfolios[] = array(
        'plugin'=>'download',
        'name'=>'File download',
        'visible'=>1);

// LTI tool config

// Sample for Piazza
// $CFG->local_autoconfig->ltitools[] = array(
//         'lti_typename'=>'Piazza',
//         'lti_toolurl'=>'https://piazza.com/connect',
//         'lti_resourcekey'=>$CFG->piazza_resourcekey,
//         'lti_password'=>$CFG->piazza_password,
//         'lti_customparameters'=>'',
//         'lti_coursevisible'=>1, // 1 = Visible as a predefined tool, 0 = Not visible (moodle recommended practice)
//         'lti_launchcontainer'=>4,  // 2 = Embed, 3 = Embed, no blocks, 5 = Existing window, 4 = New window
//         'lti_sendname'=>1, // For next 3 options, 0=No, 1=Yes, 2=Let teacher decide
//         'lti_sendemailaddr'=>1,
//         'lti_acceptgrades'=>0,
//         'lti_forcessl'=>1,
//         'lti_organizationid'=>'',
//         'lti_organizationurl'=>'');

