<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Configuration loader class
 *
 * @copyright  2016 University of Wisconsin
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class local_moodle_config {

    /**
     * Load site configuration.
     *
     * This function loads the site configuration for a given major verion and domain name.
     *
     * Config file locations:
     *   Base config:  'moodle/$majorversion/base.php'
     *   Host config:  'moodle/$majorversion/sites/$fqdn.php'  (optional)
     *
     * @param string $majorversion  e.g. "3.0"
     * @param string $fqdn  e.g. "mysite.example.com"
     */
    static function load_config($majorversion, $fqdn) {
        global $CFG;

        $configdir = __DIR__.'/moodle/'.$majorversion.'/';

        // Base config must exist
        require($configdir.'base.php');

        // Host config is optional
        $configfile = $configdir.'sites/'.$fqdn.'.php';
        if (file_exists($configfile)) {
            require($configfile);
        }
    }

}
